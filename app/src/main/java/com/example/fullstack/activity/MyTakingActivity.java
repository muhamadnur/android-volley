package com.example.fullstack.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fullstack.R;
import com.example.fullstack.adapter.MyTakingAdapter;
import com.example.fullstack.models.Product;
import com.example.fullstack.models.Taking;
import com.example.fullstack.models.TakingDetail;
import com.example.fullstack.models.User;
import com.example.fullstack.utils.Constant;
import com.google.android.material.appbar.MaterialToolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyTakingActivity extends AppCompatActivity {
    private MaterialToolbar toolbar;
    private SharedPreferences preferences;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private ArrayList<Taking> takingArrayList;
    private ArrayList<TakingDetail> takingDetailArrayList;
    private MyTakingAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_taking);
        init();
    }

    private void init() {
        preferences = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        toolbar = findViewById(R.id.toolbarMyTaking);
        refreshLayout = findViewById(R.id.swipeMyTaking);
        recyclerView = findViewById(R.id.recyclerMyTaking);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        getMyTaking();
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMyTaking();
            }
        });
    }

    private void getMyTaking() {
        refreshLayout.setRefreshing(true);
        StringRequest request = new StringRequest(Request.Method.GET, Constant.MYTAKING, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("TAKING",">>>"+response);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        takingArrayList = new ArrayList<>();
                        JSONArray array = new JSONArray(object.getString("taking"));
                        for (int i=0; i<array.length();i++){
                            JSONObject jsonObject = array.getJSONObject(i);
                            JSONObject userObject = jsonObject.getJSONObject("user");
                            JSONArray jsonArray = jsonObject.getJSONArray("takingdetail");
                            takingDetailArrayList = new ArrayList<>();
                            for (int x=0; x<jsonArray.length();x++){
                                JSONObject detailObject = jsonArray.getJSONObject(x);
                                JSONObject productObject = detailObject.getJSONObject("product");

                                Product product = new Product();
                                product.setId(productObject.getInt("id"));
                                product.setName(productObject.getString("name_product"));

                                TakingDetail takingDetail = new TakingDetail();
                                takingDetail.setId(detailObject.getInt("id"));
                                takingDetail.setProduct(product);
                                takingDetail.setQty(detailObject.getString("qty"));
                                takingDetailArrayList.add(takingDetail);
                            }

                            User user = new User();
                            user.setId(userObject.getInt("id"));
                            user.setName(userObject.getString("name"));

                            Taking taking = new Taking();
                            taking.setId(jsonObject.getInt("id"));
                            taking.setDate(jsonObject.getString("created_at"));
                            taking.setTakingDetailArrayList(takingDetailArrayList);
                            taking.setUser(user);
                            takingArrayList.add(taking);
                        }
                        adapter = new MyTakingAdapter(getApplicationContext(),takingArrayList);
                        recyclerView.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                refreshLayout.setRefreshing(false);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    public void cancelBack(View view) {
        super.onBackPressed();
    }
}