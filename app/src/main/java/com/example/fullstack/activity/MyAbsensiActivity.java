package com.example.fullstack.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fullstack.R;
import com.example.fullstack.adapter.MyPresenceAdapter;
import com.example.fullstack.models.Presence;
import com.example.fullstack.models.User;
import com.example.fullstack.utils.Constant;
import com.google.android.material.appbar.MaterialToolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyAbsensiActivity extends AppCompatActivity {
    private MaterialToolbar toolbar;
    private SharedPreferences preferences;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private ArrayList<Presence> presenceArrayList;
    private MyPresenceAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_absensi);
        init();
    }

    private void init() {
        preferences = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        recyclerView = findViewById(R.id.recyclerMyPresence);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        refreshLayout = findViewById(R.id.swipePresence);
        getMyPresence();
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMyPresence();
            }
        });
    }


    private void getMyPresence() {
        presenceArrayList = new ArrayList<>();
        refreshLayout.setRefreshing(true);
        StringRequest request = new StringRequest(Request.Method.GET, Constant.MY_PRESENCE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    Log.e("PRESENCE",">>>"+response);
                    if (object.getBoolean("success")){
                        JSONArray jsonArray = new JSONArray(object.getString("presence"));
                        for (int i=0; i<jsonArray.length();i++){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            JSONObject userObject = jsonObject.getJSONObject("user");

                            User user = new User();
                            user.setId(userObject.getInt("id"));
                            user.setName(userObject.getString("name"));

                            Presence presence = new Presence();
                            presence.setId(jsonObject.getInt("id"));
                            presence.setUser(user);
                            presence.setLatitude(jsonObject.getString("latitude"));
                            presence.setLongtitude(jsonObject.getString("longtitude"));
                            presence.setAddress(jsonObject.getString("address"));
                            presence.setDate(jsonObject.getString("created_at"));

                            presenceArrayList.add(presence);
                        }
                        adapter = new MyPresenceAdapter(getApplicationContext(),presenceArrayList);
                        recyclerView.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                refreshLayout.setRefreshing(false);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }


    public void cancelBack(View view) {
        super.onBackPressed();
    }
}