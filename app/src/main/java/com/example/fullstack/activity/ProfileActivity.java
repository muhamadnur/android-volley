package com.example.fullstack.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fullstack.R;
import com.example.fullstack.utils.Constant;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class ProfileActivity extends AppCompatActivity {
    private TextInputLayout layoutName, layoutEmail, layoutPhone, layoutGender, layoutAddress;
    private TextInputEditText txtName, txtEmail, txtPhone, txtGender, txtAddress;
    private TextView txtSelectPhoto;
    private Button btnSave;
    private CircleImageView imgProfile;
    private Bitmap bitmap;
    private AlertDialog spotDialog;
    private SharedPreferences preferences;
    private static final int GALERY_CHANGE_PROFILE = 4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
    }

    private void init() {
        preferences = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        layoutName = findViewById(R.id.layoutName);
        layoutEmail = findViewById(R.id.layoutEmail);
        layoutPhone = findViewById(R.id.layoutPhone);
        layoutGender = findViewById(R.id.layoutGender);
        layoutAddress = findViewById(R.id.layoutAddress);
        txtName = findViewById(R.id.txtEditName);
        txtEmail = findViewById(R.id.txtEditEmail);
        txtPhone = findViewById(R.id.txtEditPhone);
        txtGender = findViewById(R.id.txtEditGender);
        txtAddress = findViewById(R.id.txtEditAddress);
        txtSelectPhoto = findViewById(R.id.txtEditSelectPhoto);
        imgProfile = findViewById(R.id.imgEditUserInfo);
        btnSave = findViewById(R.id.btnEditSave);

        spotDialog = new SpotsDialog.Builder()
                .setContext(ProfileActivity.this)
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();

        Picasso.get().load(getIntent().getStringExtra("imgUrl")).into(imgProfile);
        txtName.setText(preferences.getString("name",""));
        txtEmail.setText(preferences.getString("email",""));
        txtPhone.setText(preferences.getString("phone",""));
        txtGender.setText(preferences.getString("gender",""));
        txtAddress.setText(preferences.getString("address",""));

        txtSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType("image/*");
                startActivityForResult(i, GALERY_CHANGE_PROFILE);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()){
                    updateProfile();
                }
            }
        });

    }
    private void updateProfile() {
        spotDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.EDIT_USER_INFO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("name",txtName.getText().toString().trim());
                        editor.putString("email",txtEmail.getText().toString().trim());
                        editor.putString("phone",txtPhone.getText().toString().trim());
                        editor.putString("gender",txtGender.getText().toString().trim());
                        editor.putString("address",txtAddress.getText().toString().trim());
                        editor.putString("photo",imgProfile.toString());
                        editor.apply();
                        Toast.makeText(ProfileActivity.this,"Profile Updated",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("USER",">>>"+e);
                }
                spotDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                spotDialog.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token","");
                HashMap<String,String> map = new HashMap<>();
                map.put("Authorization","Bearer "+token);
                return map;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> map = new HashMap<>();
                map.put("name",txtName.getText().toString());
                map.put("email",txtEmail.getText().toString());
                map.put("phone",txtPhone.getText().toString());
                map.put("gender",txtGender.getText().toString());
                map.put("address",txtAddress.getText().toString());
                map.put("photo",bitmapToString(bitmap));
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==GALERY_CHANGE_PROFILE && resultCode==RESULT_OK){
            Uri uri = data.getData();
            imgProfile.setImageURI(uri);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private boolean validate (){
        if (txtName.getText().toString().isEmpty()){
            layoutName.setErrorEnabled(true);
            layoutName.setError("Name is Required");
            return false;
        }
        if (txtEmail.getText().toString().isEmpty()){
            layoutEmail.setErrorEnabled(true);
            layoutEmail.setError("Email is Required");
            return false;
        }
        if (txtPhone.getText().toString().isEmpty()){
            layoutPhone.setErrorEnabled(true);
            layoutPhone.setError("Phone is Required");
            return false;
        }
        if (txtGender.getText().toString().isEmpty()){
            layoutGender.setErrorEnabled(true);
            layoutGender.setError("Gender is Required");
            return false;
        }
        if (txtAddress.getText().toString().isEmpty()){
            layoutAddress.setErrorEnabled(true);
            layoutAddress.setError("Address is Required");
            return false;
        }
        return true;
    }
    private String bitmapToString(Bitmap bitmap) {
        if (bitmap!=null){
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
            byte [] array = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(array,Base64.DEFAULT);
        }
        return "";
    }

    public void cancelBack(View view) {
        super.onBackPressed();
    }
}