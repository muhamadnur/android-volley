package com.example.fullstack.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fullstack.R;
import com.example.fullstack.adapter.MySalesAdapter;
import com.example.fullstack.models.JourneyPlan;
import com.example.fullstack.models.Market;
import com.example.fullstack.models.Product;
import com.example.fullstack.models.Sales;
import com.example.fullstack.models.SalesDetail;
import com.example.fullstack.models.Store;
import com.example.fullstack.models.User;
import com.example.fullstack.utils.Constant;
import com.google.android.material.appbar.MaterialToolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MySalesActivity extends AppCompatActivity {
    private MaterialToolbar toolbar;
    private SharedPreferences preferences;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private ArrayList<Sales> salesArrayList;
    private ArrayList<SalesDetail> salesDetailArrayList;
    private MySalesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_sales);
        init();
    }

    private void init() {
        preferences = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        toolbar = findViewById(R.id.toolbarMySales);
        refreshLayout = findViewById(R.id.swipeMySales);
        recyclerView = findViewById(R.id.recyclerMySales);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getMySales();
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMySales();
            }
        });
    }

    private void getMySales() {
        refreshLayout.setRefreshing(true);
        StringRequest request = new StringRequest(Request.Method.GET, Constant.MYSALES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        Log.e("SALES",">>>"+response);
                        salesArrayList = new ArrayList<>();
                        JSONArray jsonArray = new JSONArray(object.getString("sale"));
                        for (int i=0; i<jsonArray.length();i++){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            JSONObject userObject = jsonObject.getJSONObject("user");
                            JSONObject journeyObject = jsonObject.getJSONObject("journeyplan");
                            JSONObject storeObject = journeyObject.getJSONObject("store");
                            JSONObject marketObject = storeObject.getJSONObject("market");
                            Market market = new Market();
                            market.setId(marketObject.getInt("id"));
                            market.setName(marketObject.getString("name"));

                            Store store = new Store();
                            store.setId(storeObject.getInt("id"));
                            store.setMarket(market);
                            store.setPosition(storeObject.getString("position"));
                            store.setName(storeObject.getString("name"));
                            store.setOwner(storeObject.getString("owner"));
                            store.setPhone(storeObject.getString("phone"));

                            JourneyPlan journeyPlan = new JourneyPlan();
                            journeyPlan.setId(journeyObject.getInt("id"));
                            journeyPlan.setStore(store);

                            JSONArray detailArray = jsonObject.getJSONArray("saledetail");
                            salesDetailArrayList = new ArrayList<>();
                            for (int y=0; y<detailArray.length();y++){
                                JSONObject detailObject = detailArray.getJSONObject(y);
                                JSONObject productObject = detailObject.getJSONObject("product");

                                Product product = new Product();
                                product.setId(productObject.getInt("id"));
                                product.setName(productObject.getString("name_product"));

                                SalesDetail salesDetail = new SalesDetail();
                                salesDetail.setId(detailObject.getInt("id"));
                                salesDetail.setQty(detailObject.getString("qty"));
                                salesDetail.setProduct(product);
                                salesDetailArrayList.add(salesDetail);
                            }

                            User user = new User();
                            user.setId(userObject.getInt("id"));
                            user.setName(userObject.getString("name"));

                            Sales sales = new Sales();
                            sales.setId(jsonObject.getInt("id"));
                            sales.setUser(user);
                            sales.setJourneyPlan(journeyPlan);
                            sales.setSalesDetailArrayList(salesDetailArrayList);
                            sales.setCreated_at(jsonObject.getString("created_at"));
                            salesArrayList.add(sales);
                        }
                        adapter = new MySalesAdapter(getApplicationContext(),salesArrayList);
                        recyclerView.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                refreshLayout.setRefreshing(false);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token","");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    public void cancelBack(View view) {
        super.onBackPressed();
    }
}