package com.example.fullstack.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fullstack.R;
import com.example.fullstack.adapter.ProductAdapter;
import com.example.fullstack.models.Product;
import com.example.fullstack.models.Sales;
import com.example.fullstack.models.SalesDetail;
import com.example.fullstack.models.TakingDetail;
import com.example.fullstack.utils.Constant;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

import static com.example.fullstack.fragment.TakingFragment.takingDetailArrayList;

public class JourneyPlanActivity extends AppCompatActivity {
    private TextInputLayout layoutMarket, layoutStore, layoutOwner, layoutPhone, layoutPosition;
    private TextInputEditText txtMarket, txtStore, txtOwner, txtPhone, txtPosition;
    private ImageButton imageAdd, imageClose;
    private EditText editTextQty;
    private RecyclerView recyclerView;
    private Button btnSimpan, btnTambah;
    private ArrayList<Product> arrayList;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private AlertDialog spotDialog;
    private SharedPreferences preferences;
    ProductAdapter.ProductViewHolder holder;
    String idProduct = "";
    String idJourney = "";
    String IDProd,namaProduct, qty;
    Dialog popUpAdd;
    Spinner spinnerProduct;
    ArrayList<Product> productArrayList;
    ArrayList<String> stringArrayList = new ArrayList<>();
    ArrayList<String> idArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_plan);
        init();
        showProduct();
    }

    private void showProduct() {
        popUpAdd = new Dialog(this);
        popUpAdd.setContentView(R.layout.layout_product);
        popUpAdd.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        popUpAdd.getWindow().getAttributes().gravity = Gravity.CENTER;
        imageClose = popUpAdd.findViewById(R.id.imageClose);
        spinnerProduct = popUpAdd.findViewById(R.id.spinnerProduct);
        editTextQty = popUpAdd.findViewById(R.id.editTextQty);
        btnTambah = popUpAdd.findViewById(R.id.btnTambah);
        loadProduct();
        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpAdd.cancel();
            }
        });
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = spinnerProduct.getSelectedItem().toString();
                String qty = editTextQty.getText().toString();
                Log.e("DATA",">>>"+idProduct+" Name "+name);
                if (qty.isEmpty()) {
                    Toast.makeText(JourneyPlanActivity.this, "Qty tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                } else {
                    arrayList.add(new Product(idProduct,name, qty));
                    recyclerView.getAdapter().notifyDataSetChanged();
                    mAdapter.notifyItemInserted(arrayList.size() - 1);
                    editTextQty.setText("");
                    popUpAdd.cancel();
                }
            }
        });
    }

    private void loadProduct() {
        StringRequest request = new StringRequest(Request.Method.GET, Constant.PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("strrrrr", ">>" + response);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.optString("success").equals("true")){
                        productArrayList = new ArrayList<>();
                        JSONArray array = object.getJSONArray("product");
                        for (int i=0;i<array.length();i++){
                            Product product = new Product();
                            JSONObject jsonObject = array.getJSONObject(i);
                            product.setId(jsonObject.getInt("id"));
                            product.setName(jsonObject.getString("name_product"));
                            productArrayList.add(product);
                        }
                        for (int i=0; i<productArrayList.size();i++){
                            stringArrayList.add(productArrayList.get(i).getName().toString());
                            idArrayList.add(String.valueOf(productArrayList.get(i).getId()));
                        }
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(JourneyPlanActivity.this, android.R.layout.simple_spinner_item,stringArrayList);
                        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerProduct.setAdapter(arrayAdapter);
                        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                idProduct = idArrayList.get(position);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("ERROR",""+error);
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    private void init() {
        preferences = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        layoutMarket = findViewById(R.id.layoutMarket);
        layoutStore = findViewById(R.id.layoutStore);
        layoutOwner = findViewById(R.id.layoutOwner);
        layoutPhone = findViewById(R.id.layoutPhone);
        layoutPosition = findViewById(R.id.layoutPosition);
        txtMarket = findViewById(R.id.txtMarket);
        txtStore = findViewById(R.id.txtStore);
        txtOwner = findViewById(R.id.txtOwner);
        txtPhone = findViewById(R.id.txtPhone);
        txtPosition = findViewById(R.id.txtPosition);
        imageAdd = findViewById(R.id.imageAddProduct);
        btnSimpan = findViewById(R.id.btnSimpanPenjualan);
        Intent intent = getIntent();
        txtMarket.setText(intent.getStringExtra("MARKET"));
        txtStore.setText(intent.getStringExtra("STORE"));
        txtOwner.setText(intent.getStringExtra("OWNER"));
        txtPhone.setText(intent.getStringExtra("PHONE"));
        txtPosition.setText(intent.getStringExtra("POSITION"));
        idJourney = intent.getStringExtra("JOURNEY");
        spotDialog = new SpotsDialog.Builder()
                .setContext(JourneyPlanActivity.this)
                .setMessage("Mohon menunggu...")
                .setCancelable(false)
                .build();
//        idJourney.equals(String.valueOf(intent.getStringExtra("JOURNEY")));
        arrayList = new ArrayList<>();
        buildRecyclerView();
        imageAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpAdd.show();
            }
        });
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recyclerView.getChildCount() > 0){
                    addSale();
                }else{
                    Toast.makeText(getApplicationContext(), "Produk tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addSale() {
        spotDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.ADD_SALES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
//                        JSONObject salesObject = object.getJSONObject("sale");
//                        JSONObject userObject = salesObject.getJSONObject("user");
//                        JSONArray jsonArray = salesObject.getJSONArray("saledetail");
                        Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                spotDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                spotDialog.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("journey",idJourney);
                for (int i = 0; i < recyclerView.getChildCount(); i++) {
                    if (recyclerView.findViewHolderForLayoutPosition(i) instanceof ProductAdapter.ProductViewHolder) {
//                        Product product = new Product();
                        holder = (ProductAdapter.ProductViewHolder) recyclerView.findViewHolderForLayoutPosition(i);
                        IDProd = "product["+i+"]";
                        qty = "qty["+i+"]";
                        map.put(IDProd,holder.txtIDProduct.getText().toString());
                        map.put(qty, holder.txtQty.getText().toString());
                    }
                }
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    private void buildRecyclerView() {
        recyclerView = findViewById(R.id.recyclerProduct);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        mAdapter = new ProductAdapter(arrayList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    public void cancelBack(View view) {
        super.onBackPressed();
    }
}