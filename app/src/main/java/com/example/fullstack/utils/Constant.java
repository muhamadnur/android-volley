package com.example.fullstack.utils;

import com.example.fullstack.models.Taking;

public class Constant {
    public static final String URL = "http://app.digas.site/public/";
//    public static final String URL = "http://af5ae78e3f4e.ap.ngrok.io/";
    public static final String HOME = URL + "api/";
    public static final String LOGIN = HOME + "login";
    public static final String LOGOUT = HOME + "logout";
    public static final String EDIT_USER_INFO = HOME + "update_user";
    public static final String PROFILE = HOME + "my_profile";

    //product
    public static final String PRODUCT = HOME + "product";

    //presence
    public static final String PRESENCE = HOME + "presence";
    public static final String ADD_PRESENCE = PRESENCE + "/create";
    public static final String MY_PRESENCE = PRESENCE + "/my_presence";

    //taking
    public static final String TAKING = HOME + "taking";
    public static final String ADD_TAKING = TAKING + "/create";
    public static final String DETAIL = HOME + "takingdetail";
    public static final String MY_TAKING = DETAIL + "/my_taking";
    public static final String MYTAKING = TAKING + "/my_taking";


    //market
    public static final String MARKET = HOME + "market";

    //journey
    public static final String JOURNEY = HOME + "journey";


    //sales
    public static final String SALES = HOME + "sales";
    public static final String MY_SALES = SALES + "/my_sales";
    public static final String MYSALES = SALES + "/mysales";
    public static final String ADD_SALES = SALES + "/create";

    //laporan
    public static final String JOURNEY_PDF = JOURNEY + "/create/pdf";
    public static final String TAKING_PDF = TAKING + "/create/pdf";
    public static final String PRESENCE_PDF = PRESENCE + "/create/pdf";
    public static final String SALES_PDF = SALES + "/create/pdf";

}
