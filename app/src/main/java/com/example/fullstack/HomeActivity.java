package com.example.fullstack;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.fullstack.fragment.HomeFragment;
import com.example.fullstack.fragment.PresenceFragment;
import com.example.fullstack.fragment.ProfileFragment;
import com.example.fullstack.fragment.TakingFragment;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends AppCompatActivity {
    private BottomNavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        navigationView = findViewById(R.id.bottom_nav);
        navigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        loadFragment(new HomeFragment());
    }
    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .addToBackStack(null)
                    .commit();
            return true;
        }
        return false;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()){
                case R.id.navigation_penjualan:
                    fragment = new HomeFragment();
                    break;
                case R.id.navigation_pengambilan:
                    fragment = new TakingFragment();
                    break;
                case R.id.navigation_presence:
                    fragment = new PresenceFragment();
                    break;
                case R.id.navigation_account:
                    fragment = new ProfileFragment();
                    break;
            }
            return loadFragment(fragment);
        }
    };

    boolean isBackButtonClicked = false;

    @Override
    public void onBackPressed() {
        if (isBackButtonClicked){
            super.onBackPressed();
            return;
        }
        this.isBackButtonClicked = false;
//        Toast.makeText(this,"Please click BACK again to exit!", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onPostResume() {
        isBackButtonClicked = false;
        super.onPostResume();
    }

}