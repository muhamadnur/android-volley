package com.example.fullstack.Interface;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(View v, int pos);
}
