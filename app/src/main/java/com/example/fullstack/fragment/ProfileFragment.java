package com.example.fullstack.fragment;

import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fullstack.AuthActivity;
import com.example.fullstack.HomeActivity;
import com.example.fullstack.R;
import com.example.fullstack.activity.MyAbsensiActivity;
import com.example.fullstack.activity.MySalesActivity;
import com.example.fullstack.activity.MyTakingActivity;
import com.example.fullstack.activity.ProfileActivity;
import com.example.fullstack.models.Sales;
import com.example.fullstack.utils.Constant;
import com.google.android.material.appbar.MaterialToolbar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class ProfileFragment extends Fragment {

    private View view;
    private MaterialToolbar toolbar;
    private CircleImageView imgProfile;
    private TextView txtName, txtEmail, txtPhone, txtTotal,txtTaking;
    private ImageButton imgButtonEdit;
    private SwipeRefreshLayout refreshLayout;
    private SharedPreferences preferences;
    private String imgUrl = "";
    private android.app.AlertDialog spotDialog;
    private LinearLayout linearAbsensi, linearTaking, linearSales,laporanJourney,laporanAbsensi,laporanPengambilan,laporanPenjualan;
    private ArrayList<Sales> arrayList;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        init();
        return view;
    }

    private void init() {
        preferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        toolbar = view.findViewById(R.id.toolbarProfile);
        setHasOptionsMenu(true);
        ((HomeActivity) getContext()).setSupportActionBar(toolbar);
        imgProfile = view.findViewById(R.id.imgAccountProfile);
        txtName = view.findViewById(R.id.txtAccountName);
        txtEmail = view.findViewById(R.id.txtAccountEmail);
        imgButtonEdit = view.findViewById(R.id.imgButtonEdit);
        txtPhone = view.findViewById(R.id.txtPhone);
        txtTotal = view.findViewById(R.id.txtTotal);
        txtTaking = view.findViewById(R.id.txtTaking);
        refreshLayout = view.findViewById(R.id.swipeProfile);
        linearAbsensi = view.findViewById(R.id.linearAbsensi);
        linearTaking = view.findViewById(R.id.linearTaking);
        linearSales = view.findViewById(R.id.linearSales);
        laporanAbsensi = view.findViewById(R.id.laporanAbsensi);
        laporanPengambilan = view.findViewById(R.id.laporanPengambilan);
        laporanPenjualan = view.findViewById(R.id.laporanPenjualan);
        laporanJourney = view.findViewById(R.id.laporanJourney);
        spotDialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
//        getProfile();
        getMySales();
        getMyTaking();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMySales();
                getMyTaking();
//                getProfile();
            }
        });
        imgButtonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(((HomeActivity)getContext()), ProfileActivity.class);
                i.putExtra("imgUrl",imgUrl);
                startActivity(i);
            }
        });
        linearAbsensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(((HomeActivity)getContext()), MyAbsensiActivity.class);
                startActivity(intent);
            }
        });
        linearTaking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(((HomeActivity)getContext()), MyTakingActivity.class);
                startActivity(intent);
            }
        });
        linearSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(((HomeActivity)getContext()), MySalesActivity.class);
                startActivity(intent);
            }
        });
        laporanJourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               downloadJourney();
            }
        });
        laporanAbsensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLaporanAbsensi();
            }
        });
        laporanPengambilan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLaporanPengambilan();
            }
        });
        laporanPenjualan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLaporanPenjualan();
            }
        });
    }

    private void getLaporanPenjualan() {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(Constant.JOURNEY_PDF));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setTitle("Download");
        request.setDescription("Downloading file...");
        request.allowScanningByMediaScanner();
        request.setMimeType("application/pdf");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,"Data Absensi_"+System.currentTimeMillis()+".pdf");

        //get download service and enque file
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    private void getLaporanPengambilan() {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(Constant.JOURNEY_PDF));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setTitle("Download");
        request.setDescription("Downloading file...");
        request.allowScanningByMediaScanner();
        request.setMimeType("application/pdf");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,"Data Absensi_"+System.currentTimeMillis()+".pdf");

        //get download service and enque file
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    private void getLaporanAbsensi() {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(Constant.JOURNEY_PDF));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setTitle("Download");
        request.setDescription("Downloading file...");
        request.allowScanningByMediaScanner();
        request.setMimeType("application/pdf");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,"Data Absensi_"+System.currentTimeMillis()+".pdf");

        //get download service and enque file
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    private void downloadJourney() {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(Constant.JOURNEY_PDF));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setTitle("Download");
        request.setDescription("Downloading file...");
        request.allowScanningByMediaScanner();
        request.setMimeType("application/pdf");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,"Data Journey_"+System.currentTimeMillis()+".pdf");

        //get download service and enque file
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    private void getMyTaking() {
        refreshLayout.setRefreshing(true);
        StringRequest request = new StringRequest(Request.Method.GET, Constant.MY_TAKING, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        JSONArray jsonArray = object.getJSONArray("detail");
                        txtTaking.setText(object.getString("total"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                refreshLayout.setRefreshing(false);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(request);
    }

    private void getMySales() {
        refreshLayout.setRefreshing(true);
        StringRequest request = new StringRequest(Request.Method.GET, Constant.MY_SALES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        JSONArray detail = object.getJSONArray("detail");
                        JSONObject userObject = object.getJSONObject("user");
//                        JSONObject total = object.getJSONObject("total");
                        txtTotal.setText(object.getString("total"));
                        txtName.setText(userObject.getString("name"));
                        txtEmail.setText(userObject.getString("email"));
                        txtPhone.setText(userObject.getString("phone"));
                        Picasso.get().load(Constant.URL + "storage/profiles/" + userObject.getString("photo")).into(imgProfile);
                        imgUrl = Constant.URL+"storage/profiles/"+userObject.getString("photo");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                refreshLayout.setRefreshing(false);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(request);
    }

//    private void getProfile() {
//        Picasso.get().load(Constant.URL + "storage/profiles/" + preferences.getString("photo","")).into(imgProfile);
//        txtName.setText(preferences.getString("name",""));
//        txtEmail.setText(preferences.getString("email",""));
//        txtPhone.setText(preferences.getString("phone",""));
//        imgUrl = Constant.URL+"storage/profiles/"+preferences.getString("photo","");
//        refreshLayout.setRefreshing(false);
//    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_account,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_logout:{
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Apakah anda ingin keluar?");
                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });
                builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        spotDialog.show();
        StringRequest request = new StringRequest(Request.Method.GET, Constant.LOGOUT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();
                        startActivity(new Intent((HomeActivity)getContext(), AuthActivity.class));
                        ((HomeActivity)getContext()).finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                spotDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                spotDialog.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token","");
                HashMap<String,String> map = new HashMap<>();
                map.put("Authorization","Bearer "+token);
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(request);
    }
}