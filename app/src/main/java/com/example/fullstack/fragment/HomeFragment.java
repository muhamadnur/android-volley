package com.example.fullstack.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fullstack.HomeActivity;
import com.example.fullstack.R;
import com.example.fullstack.adapter.JourneyAdapter;
import com.example.fullstack.models.JourneyPlan;
import com.example.fullstack.models.Market;
import com.example.fullstack.models.Store;
import com.example.fullstack.models.User;
import com.example.fullstack.utils.Constant;
import com.google.android.material.appbar.MaterialToolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class HomeFragment extends Fragment {
    private View view;
    private MaterialToolbar toolbar;
    private SharedPreferences preferences;
    private SwipeRefreshLayout refreshLayout;
    public static RecyclerView recyclerView;
    public static ArrayList<JourneyPlan> journeyPlanArrayList;
    private JourneyAdapter journeyAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        init();
        return view;
    }

    private void init() {
        preferences =getContext().getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        recyclerView = view.findViewById(R.id.recyclerHome);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        refreshLayout = view.findViewById(R.id.swipeHome);
        toolbar = view.findViewById(R.id.toolbarHome);
        setHasOptionsMenu(true);
        ((HomeActivity) getContext()).setSupportActionBar(toolbar);
        getJourney();
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getJourney();
            }
        });
    }

    private void getJourney() {
        journeyPlanArrayList = new ArrayList<>();
        refreshLayout.setRefreshing(true);
        StringRequest request = new StringRequest(Request.Method.GET, Constant.JOURNEY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        JSONArray array = new JSONArray(object.getString("journey"));
                        for (int i=0; i<array.length(); i++){
                            JSONObject journeyObject = array.getJSONObject(i);
                            JSONObject userObject = journeyObject.getJSONObject("user");
                            JSONObject storeObject = journeyObject.getJSONObject("store");

                            JSONObject marketObject = storeObject.getJSONObject("market");
                            Market market = new Market();
                            market.setId(marketObject.getInt("id"));
                            market.setName(marketObject.getString("name"));

                            User user = new User();
                            user.setId(userObject.getInt("id"));
                            user.setName(userObject.getString("name"));

                            Store store = new Store();
                            store.setId(storeObject.getInt("id"));
                            store.setMarket(market);
                            store.setName(storeObject.getString("name"));
                            store.setOwner(storeObject.getString("owner"));
                            store.setPhone(storeObject.getString("phone"));
                            store.setPosition(storeObject.getString("position"));

                            JourneyPlan journeyPlan = new JourneyPlan();
                            journeyPlan.setId(journeyObject.getInt("id"));
                            journeyPlan.setUser(user);
                            journeyPlan.setStore(store);
                            journeyPlan.setStartDate(journeyObject.getString("start_date"));
                            journeyPlan.setEndDate(journeyObject.getString("end_date"));

                            journeyPlanArrayList.add(journeyPlan);
                        }
                        journeyAdapter = new JourneyAdapter(getContext(),journeyPlanArrayList);
                        recyclerView.setAdapter(journeyAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                refreshLayout.setRefreshing(false);
            }
        }){
            //provide token in header
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(request);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                journeyAdapter.getFilter().filter(newText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }
}