package com.example.fullstack.fragment;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fullstack.HomeActivity;
import com.example.fullstack.R;
import com.example.fullstack.activity.TakingActivity;
import com.example.fullstack.adapter.TakingAdapter;
import com.example.fullstack.models.Product;
import com.example.fullstack.models.Taking;
import com.example.fullstack.models.TakingDetail;
import com.example.fullstack.models.User;
import com.example.fullstack.utils.Constant;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class TakingFragment extends Fragment {
    private View view;
    private MaterialToolbar toolbar;
    private FloatingActionButton fab;
    private SharedPreferences preferences;
    private SwipeRefreshLayout refreshLayout;
    public static RecyclerView recyclerView;
    public static ArrayList<TakingDetail> takingDetailArrayList;
    public static ArrayList<Taking> takingArrayList;
    private TakingAdapter takingAdapter;
    Uri imgUri;
    private static final int GALLERY_ADD_POST = 1001;
    private static final int PERMISSION_CODE = 1000;
    public TakingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_taking, container, false);
        init();
        return view;
    }

    private void init() {
        preferences =getContext().getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        recyclerView = view.findViewById(R.id.recyclerPengambilan);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        refreshLayout = view.findViewById(R.id.swipePengambilan);
        toolbar = view.findViewById(R.id.toolbarPengambilan);
        fab = view.findViewById(R.id.fabPengambilan);
        setHasOptionsMenu(true);
        ((HomeActivity) getContext()).setSupportActionBar(toolbar);

        String role = preferences.getString("role_id","");
        if (role.equals("1")){
            fab.setVisibility(View.VISIBLE);
        }else if (role.equals("2")){
            fab.setVisibility(View.VISIBLE);
        }else if (role.equals("3")){
            fab.setVisibility(View.VISIBLE);
        }else{
            fab.setVisibility(View.GONE);
        }
        getTaking();
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getTaking();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED || getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                        String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        requestPermissions(permission, PERMISSION_CODE);
                    } else {
                        openCamera();
                    }
                } else {
                    openCamera();
                }
            }
        });
    }

    private void getTaking() {

        refreshLayout.setRefreshing(true);
        StringRequest request = new StringRequest(Request.Method.GET, Constant.TAKING, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        takingArrayList = new ArrayList<>();
                        JSONArray array = new JSONArray(object.getString("taking"));
                        for (int i =0; i < array.length(); i++) {
                            JSONObject takingObject = array.getJSONObject(i);
                            JSONObject userObject = takingObject.getJSONObject("user");

                            JSONArray jsonArray = takingObject.getJSONArray("takingdetail");
                            if (jsonArray != null && jsonArray.length()>0){
                                takingDetailArrayList = new ArrayList<>();
                                for (int x=0;x<jsonArray.length();x++){
                                    JSONObject detailObject = jsonArray.getJSONObject(x);
                                    JSONObject productObject = detailObject.getJSONObject("product");

                                    Product product = new Product();
                                    product.setId(productObject.getInt("id"));
                                    product.setName(productObject.getString("name_product"));

                                    TakingDetail takingDetail = new TakingDetail();
                                    takingDetail.setId(detailObject.getInt("id"));
                                    takingDetail.setProduct(product);
                                    takingDetail.setQty(detailObject.getString("qty"));
                                    takingDetailArrayList.add(takingDetail);
                                }

                            }
                            User user = new User();
                            user.setId(userObject.getInt("id"));
                            user.setName(userObject.getString("name"));
                            user.setPhoto(userObject.getString("photo"));

                            Taking taking = new Taking();
                            taking.setUser(user);
                            taking.setId(takingObject.getInt("id"));
                            taking.setTakingDetailArrayList(takingDetailArrayList);
                            taking.setDate(takingObject.getString("created_at"));
                            taking.setPhoto(takingObject.getString("photo"));

                            takingArrayList.add(taking);
                        }
                        takingAdapter = new TakingAdapter(getContext(),takingArrayList);
                        recyclerView.setAdapter(takingAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                refreshLayout.setRefreshing(false);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = preferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(request);
    }

    private void openCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
        imgUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        startActivityForResult(cameraIntent, GALLERY_ADD_POST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Intent i = new Intent(((HomeActivity) getContext()), TakingActivity.class);
            i.setData(imgUri);
            startActivity(i);
        }
    }
}