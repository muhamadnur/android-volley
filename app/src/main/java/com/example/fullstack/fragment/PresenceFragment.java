package com.example.fullstack.fragment;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fullstack.HomeActivity;
import com.example.fullstack.R;
import com.example.fullstack.activity.PresenceActivity;
import com.example.fullstack.adapter.PresenceAdapter;
import com.example.fullstack.models.Presence;
import com.example.fullstack.models.User;
import com.example.fullstack.utils.Constant;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static android.app.Activity.RESULT_OK;


public class PresenceFragment extends Fragment {
    private View view;
    private String latitude, longtitude, address;
    private MaterialToolbar toolbar;
    private FloatingActionButton fab;
    private SharedPreferences sharedPreferences;
    private SwipeRefreshLayout refreshLayout;
    public static RecyclerView recyclerViewPresence;
    ;
    public static ArrayList<Presence> arrayListPresence;
    private PresenceAdapter presenceAdapter;
    Uri imgUri;
    private static final int GALLERY_ADD_POST = 1001;
    private static final int PERMISSION_CODE = 1000;
    private FusedLocationProviderClient fusedLocationProviderClient;

    public PresenceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_presence, container, false);
        init();
        return view;
    }

    private void init() {
        sharedPreferences = getContext().getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        recyclerViewPresence = view.findViewById(R.id.recyclerPresence);
        recyclerViewPresence.setHasFixedSize(true);
        recyclerViewPresence.setLayoutManager(new LinearLayoutManager(getContext()));
        refreshLayout = view.findViewById(R.id.swipePresence);
        toolbar = view.findViewById(R.id.toolbarPresence);
        fab = view.findViewById(R.id.fab);
        setHasOptionsMenu(true);
        ((HomeActivity) getContext()).setSupportActionBar(toolbar);

        String role = sharedPreferences.getString("role_id","");
        if (role.equals("1")){
            fab.setVisibility(View.VISIBLE);
        }else if (role.equals("2")){
            fab.setVisibility(View.VISIBLE);
        }else if (role.equals("3")){
            fab.setVisibility(View.VISIBLE);
        }else{
            fab.setVisibility(View.GONE);
        }
        getPresence();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPresence();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED || getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                        String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        requestPermissions(permission, PERMISSION_CODE);
                    } else {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            openCamera();
                        } else {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
                        }
                    }
                } else {
                    openCamera();
                }
            }
        });
    }


    private void openCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
        imgUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        startActivityForResult(cameraIntent, GALLERY_ADD_POST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Intent i = new Intent(((HomeActivity)getContext()), PresenceActivity.class);
            i.setData(imgUri);
            startActivity(i);
        }
    }

    private void getPresence() {
        arrayListPresence = new ArrayList<>();
        refreshLayout.setRefreshing(true);
        StringRequest request = new StringRequest(Request.Method.GET, Constant.PRESENCE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")) {
                        JSONArray array = new JSONArray(object.getString("presence"));
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject presenceObject = array.getJSONObject(i);
                            JSONObject userObject = presenceObject.getJSONObject("user");

                            User user = new User();
                            user.setId(userObject.getInt("id"));
                            user.setName(userObject.getString("name"));
                            user.setPhoto(userObject.getString("photo"));

                            Presence presence = new Presence();
                            presence.setUser(user);
                            presence.setId(presenceObject.getInt("id"));
                            presence.setLatitude(presenceObject.getString("latitude"));
                            presence.setLongtitude(presenceObject.getString("longtitude"));
                            presence.setAddress(presenceObject.getString("address"));
                            presence.setDate(presenceObject.getString("created_at"));
                            presence.setPhoto(presenceObject.getString("photo"));

                            arrayListPresence.add(presence);
                        }
                        presenceAdapter = new PresenceAdapter(getContext(), arrayListPresence);
                        recyclerViewPresence.setAdapter(presenceAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                refreshLayout.setRefreshing(false);
            }
        }) {
            //provide token in header
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = sharedPreferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        RequestQueue queue = MySingleton.getInstance(getContext().getApplicationContext()).getRequestQueue();
        RequestQueue queue = Volley.newRequestQueue(getContext());
//        MySingleton.getInstance(getContext()).addToRequestQueue(request);
        queue.add(request);
    }

}