package com.example.fullstack;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.fullstack.utils.Constant;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class AuthActivity extends AppCompatActivity {
    private TextInputLayout layoutEmail, layoutPassword;
    private TextInputEditText editTextEmail, editTextPassword;
    private Button btnSignIn;
    private AlertDialog spotDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        init();
    }

    private void init() {
        layoutEmail = (TextInputLayout) findViewById(R.id.layoutEmail);
        layoutPassword = (TextInputLayout) findViewById(R.id.txtLayoutPassword);
        editTextEmail = (TextInputEditText) findViewById(R.id.txtEmail);
        editTextPassword = (TextInputEditText) findViewById(R.id.txtEditTextPassword);
        btnSignIn = findViewById(R.id.btnSignIn);
        spotDialog = new SpotsDialog.Builder()
                .setContext(AuthActivity.this)
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validate fields first
                if (validate()) {
                    login();
                }
            }
        });
    }

    private void login() {
        spotDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constant.LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        JSONObject user = object.getJSONObject("user");
                        //make shared preference user
                        SharedPreferences userPref = getApplicationContext().getSharedPreferences("user", MODE_PRIVATE);
                        SharedPreferences.Editor editor = userPref.edit();
                        editor.putString("token", object.getString("token"));
                        editor.putInt("id", user.getInt("id"));
                        editor.putString("role_id", user.getString("role_id"));
                        editor.putString("name", user.getString("name"));
                        editor.putString("email", user.getString("email"));
                        editor.putString("phone", user.getString("phone"));
                        editor.putString("gender", user.getString("gender"));
                        editor.putString("address", user.getString("address"));
                        editor.putString("photo", user.getString("photo"));
//                        editor.putBoolean("isLogIn",true);
                        editor.apply();
                        startActivity(new Intent(AuthActivity.this,HomeActivity.class));
                        Toast.makeText(getApplicationContext(), "Login Success", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_SHORT).show();
                }
                spotDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                spotDialog.dismiss();
                Toast.makeText(getApplicationContext(), "" + error, Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("email", editTextEmail.getText().toString().trim());
                map.put("password", editTextPassword.getText().toString());
                return map;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);
    }

    private boolean validate() {
        if (editTextEmail.getText().toString().isEmpty()) {
            layoutEmail.setErrorEnabled(true);
            layoutEmail.setError("Email is Required");
            return false;
        }
        if (editTextPassword.getText().toString().length() < 6) {
            layoutPassword.setErrorEnabled(true);
            layoutPassword.setError("Required at least 6 characters");
            return false;
        }
        return true;
    }
}