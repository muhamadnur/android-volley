package com.example.fullstack.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fullstack.Interface.ItemClickListener;
import com.example.fullstack.R;
import com.example.fullstack.models.Presence;

import java.util.ArrayList;
import java.util.List;

public class MyPresenceAdapter extends RecyclerView.Adapter<MyPresenceAdapter.PresenceHolder>{
    private Context context;
    private ArrayList<Presence> presenceArrayList;
    private ArrayList<Presence> listAll;
    private SharedPreferences preferences;

    public MyPresenceAdapter(Context context, ArrayList<Presence> presenceArrayList) {
        this.context = context;
        this.presenceArrayList = presenceArrayList;
        this.listAll = new ArrayList<>(presenceArrayList);
        preferences = context.getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
    }


    @NonNull
    @Override
    public PresenceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_my_presence, parent, false);
        return new MyPresenceAdapter.PresenceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PresenceHolder holder, int position) {
        final Presence presence = presenceArrayList.get(position);
        holder.txtName.setText(presence.getUser().getName());
        holder.txtTanggal.setText(presence.getDate());
        holder.txtLatitude.setText(presence.getLatitude());
        holder.txtLongtitude.setText(presence.getLongtitude());
        holder.txtAddress.setText(presence.getAddress());
    }

    @Override
    public int getItemCount() {
        return presenceArrayList.size();
    }

    class PresenceHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView txtName, txtTanggal, txtLatitude, txtLongtitude,txtAddress;
        ItemClickListener itemClickListener;

        public PresenceHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
            txtLatitude = itemView.findViewById(R.id.txtLatitude);
            txtLongtitude = itemView.findViewById(R.id.txtLongtitude);
            txtAddress = itemView.findViewById(R.id.txtAddress);
        }
        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }
    }
}
