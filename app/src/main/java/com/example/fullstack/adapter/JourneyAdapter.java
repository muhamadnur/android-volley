package com.example.fullstack.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fullstack.Interface.ItemClickListener;
import com.example.fullstack.R;
import com.example.fullstack.activity.JourneyPlanActivity;
import com.example.fullstack.models.JourneyPlan;

import java.util.ArrayList;
import java.util.Collection;

public class JourneyAdapter extends RecyclerView.Adapter<JourneyAdapter.JourneyHolder>{

    private Context context;
    ArrayList<JourneyPlan> journeyPlanArrayList;
    ArrayList<JourneyPlan> listAll;
    private SharedPreferences preferences;

    public JourneyAdapter(Context context, ArrayList<JourneyPlan> journeyPlanArrayList) {
        this.context = context;
        this.journeyPlanArrayList = journeyPlanArrayList;
        this.listAll = new ArrayList<>(journeyPlanArrayList);
        preferences = context.getApplicationContext().getSharedPreferences("user",Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public JourneyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_journey,parent,false);
        return new JourneyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JourneyHolder holder, int position) {
        JourneyPlan journeyPlan = journeyPlanArrayList.get(position);
        holder.txtName.setText(journeyPlan.getStore().getName());
        holder.txtOwner.setText(journeyPlan.getStore().getOwner());
        holder.txtPhone.setText(journeyPlan.getStore().getPhone());
        holder.txtMarket.setText(journeyPlan.getStore().getMarket().getName());
        holder.txtPosition.setText(journeyPlan.getStore().getPosition());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, JourneyPlanActivity.class);
                intent.putExtra("JOURNEY",String.valueOf(journeyPlan.getId()));
                intent.putExtra("MARKET",String.valueOf(journeyPlan.getStore().getMarket().getName()));
                intent.putExtra("STORE",String.valueOf(journeyPlan.getStore().getName()));
                intent.putExtra("OWNER",String.valueOf(journeyPlan.getStore().getOwner()));
                intent.putExtra("PHONE",String.valueOf(journeyPlan.getStore().getPhone()));
                intent.putExtra("POSITION",String.valueOf(journeyPlan.getStore().getPosition()));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return journeyPlanArrayList.size();
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<JourneyPlan> filteredList = new ArrayList<>();
            if (constraint.toString().isEmpty()) {
                filteredList.addAll(listAll);
            } else {
                for (JourneyPlan journeyPlan : listAll) {
                    if (journeyPlan.getStore().getName().toLowerCase().contains(constraint.toString().toLowerCase()) |
                            journeyPlan.getStore().getMarket().getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filteredList.add(journeyPlan);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            journeyPlanArrayList.clear();
            journeyPlanArrayList.addAll((Collection<? extends JourneyPlan>) results.values);
            notifyDataSetChanged();
        }
    };

    public Filter getFilter() {
        return filter;
    }

    class JourneyHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ItemClickListener itemClickListener;
        TextView txtName, txtOwner, txtPhone, txtPosition,txtMarket;
        public JourneyHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtOwner = itemView.findViewById(R.id.txtOwner);
            txtPhone = itemView.findViewById(R.id.txtPhone);
            txtPosition = itemView.findViewById(R.id.txtPosition);
            txtMarket = itemView.findViewById(R.id.txtMarket);
        }

        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }
    }
}
