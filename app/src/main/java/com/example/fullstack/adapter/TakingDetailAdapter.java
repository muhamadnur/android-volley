package com.example.fullstack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fullstack.R;
import com.example.fullstack.models.TakingDetail;

import java.util.ArrayList;

public class TakingDetailAdapter extends RecyclerView.Adapter<TakingDetailAdapter.ViewHolder> {
    Context context;
    ArrayList<TakingDetail> takingDetailArrayList;

    public TakingDetailAdapter(Context context, ArrayList<TakingDetail> takingDetailArrayList) {
        this.context = context;
        this.takingDetailArrayList = takingDetailArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pengambilan_detail, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TakingDetail takingDetail = takingDetailArrayList.get(position);
        holder.txtProduct.setText(takingDetail.getProduct().getName());
//        holder.txtProduct.setText(takingDetail.getProduct().getName());
        holder.txtQty.setText(takingDetail.getQty());
    }

    @Override
    public int getItemCount() {
        return takingDetailArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtProduct;
        TextView txtQty;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtProduct = itemView.findViewById(R.id.txtNameProduct);
            txtQty = itemView.findViewById(R.id.txtQty);
        }
    }
}
