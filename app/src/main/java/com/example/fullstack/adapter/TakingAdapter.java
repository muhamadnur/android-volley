package com.example.fullstack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fullstack.Interface.ItemClickListener;
import com.example.fullstack.R;
import com.example.fullstack.models.Taking;
import com.example.fullstack.models.TakingDetail;
import com.example.fullstack.utils.Constant;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TakingAdapter extends RecyclerView.Adapter<TakingAdapter.ViewHolder>{
    Context context;
    ArrayList<Taking> takingArrayList;

    public TakingAdapter(Context context, ArrayList<Taking> takingArrayList) {
        this.context = context;
        this.takingArrayList = takingArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pengambilan, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Taking taking = takingArrayList.get(position);
        holder.txtName.setText(taking.getUser().getName());
        holder.txtDate.setText(taking.getDate());

        ArrayList<TakingDetail> takingDetails = taking.getTakingDetailArrayList();
        holder.recyclerView.setHasFixedSize(true);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        TakingDetailAdapter takingDetailAdapter = new TakingDetailAdapter(context,takingDetails);
        holder.recyclerView.setAdapter(takingDetailAdapter);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
                View view = LayoutInflater.from(v.getRootView().getContext()).inflate(R.layout.layout_show_picture_presence, null);
                ImageView imagePresence;
                ImageButton imageClose;
                TextView tvPhoto;
                tvPhoto = view.findViewById(R.id.tvPhoto);
                tvPhoto.setText("Foto Faktur");
                imagePresence = view.findViewById(R.id.imagePresence);
                imageClose = view.findViewById(R.id.imageClose);
                Picasso.get().load(Constant.URL + "storage/taking/" + taking.getPhoto()).placeholder(R.drawable.loading).error(R.drawable.no_image).into(imagePresence);

                imageClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.cancel();
                    }
                });
                bottomSheetDialog.setContentView(view);
                bottomSheetDialog.setCancelable(true);
                bottomSheetDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return takingArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView txtName, txtDate, txtFoto;
        private RecyclerView recyclerView;
        ItemClickListener itemClickListener;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtName = itemView.findViewById(R.id.txtName);
            txtFoto = itemView.findViewById(R.id.txtLihatFoto);
            recyclerView = itemView.findViewById(R.id.recyclerPengambilanBarang);
            txtFoto.setOnClickListener(this);
        }
        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }
        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }
    }
}
