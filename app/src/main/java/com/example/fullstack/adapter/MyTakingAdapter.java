package com.example.fullstack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fullstack.R;
import com.example.fullstack.models.Taking;
import com.example.fullstack.models.TakingDetail;

import java.util.ArrayList;

public class MyTakingAdapter extends RecyclerView.Adapter<MyTakingAdapter.TakingHolder>{
    Context context;
    ArrayList<Taking> takingArrayList;

    public MyTakingAdapter(Context context, ArrayList<Taking> takingArrayList) {
        this.context = context;
        this.takingArrayList = takingArrayList;
    }

    @NonNull
    @Override
    public TakingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_my_taking, parent, false);
        return new MyTakingAdapter.TakingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TakingHolder holder, int position) {
        Taking taking = takingArrayList.get(position);
        holder.txtName.setText(taking.getUser().getName());
        holder.txtDate.setText(taking.getDate());
        ArrayList<TakingDetail> takingDetails = taking.getTakingDetailArrayList();
        holder.recyclerMyTaking.setHasFixedSize(true);
        holder.recyclerMyTaking.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        TakingDetailAdapter takingDetailAdapter = new TakingDetailAdapter(context,takingDetails);
//        MyTakingDetailAdapter myTakingDetailAdapter = new MyTakingDetailAdapter(context, takingDetails);
        holder.recyclerMyTaking.setAdapter(takingDetailAdapter);
    }

    @Override
    public int getItemCount() {
        return takingArrayList.size();
    }

    class TakingHolder extends RecyclerView.ViewHolder{
        TextView txtName, txtDate;
        RecyclerView recyclerMyTaking;
        public TakingHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtDate = itemView.findViewById(R.id.txtDate);
            recyclerMyTaking = itemView.findViewById(R.id.recyclerMyTaking);
        }
    }
}
