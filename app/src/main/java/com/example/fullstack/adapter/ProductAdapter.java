package com.example.fullstack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fullstack.R;
import com.example.fullstack.models.Product;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder>{
    private ArrayList<Product> productArrayList;

    public ProductAdapter(ArrayList<Product> productArrayList) {
        this.productArrayList = productArrayList;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_item, parent, false);
        ProductViewHolder productViewHolder = new ProductViewHolder(view);
        return productViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        final Product product = productArrayList.get(position);
        holder.txtIDProduct.setText(product.getIdProduct());
        holder.txtNameProduct.setText(product.getName());
        holder.txtQty.setText(product.getQty());
        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = productArrayList.indexOf(product);
                productArrayList.remove(pos);
                notifyItemRemoved(pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder{
        public TextView txtNameProduct,txtQty,txtIDProduct;
        public ImageButton imageDelete;
        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            txtIDProduct = itemView.findViewById(R.id.txtIDProduct);
            txtNameProduct = itemView.findViewById( R.id.txtNameProduct);
            txtQty = itemView.findViewById(R.id.txtQty);
            imageDelete = itemView.findViewById(R.id.imageDelete);
        }
    }
}
