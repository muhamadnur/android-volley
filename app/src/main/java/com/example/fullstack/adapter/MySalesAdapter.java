package com.example.fullstack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fullstack.R;
import com.example.fullstack.models.Sales;
import com.example.fullstack.models.SalesDetail;

import java.util.ArrayList;

public class MySalesAdapter extends RecyclerView.Adapter<MySalesAdapter.SalesHolder>{
    Context context;
    ArrayList<Sales> salesArrayList;

    public MySalesAdapter(Context context, ArrayList<Sales> salesArrayList) {
        this.context = context;
        this.salesArrayList = salesArrayList;
    }

    @NonNull
    @Override
    public SalesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_my_sales,parent, false);
        SalesHolder salesHolder = new SalesHolder(view);
        return salesHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SalesHolder holder, int position) {
        Sales sales = salesArrayList.get(position);
        holder.txtNamaPasar.setText(sales.getJourneyPlan().getStore().getMarket().getName());
        holder.txtPosition.setText(sales.getJourneyPlan().getStore().getPosition());
        holder.txtNamaToko.setText(sales.getJourneyPlan().getStore().getName());
        holder.txtNamaPemilik.setText(sales.getJourneyPlan().getStore().getOwner());
        holder.txtTanggal.setText(sales.getCreated_at());
        holder.txtPhone.setText(sales.getJourneyPlan().getStore().getPhone());
        ArrayList<SalesDetail> salesDetails = sales.getSalesDetailArrayList();
        holder.recyclerView.setHasFixedSize(true);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false));
        MySalesDetailAdapter mySalesDetailAdapter = new MySalesDetailAdapter(context,salesDetails);
        holder.recyclerView.setAdapter(mySalesDetailAdapter);
    }

    @Override
    public int getItemCount() {
        return salesArrayList.size();
    }

    class SalesHolder extends RecyclerView.ViewHolder{
        TextView txtNamaPasar, txtPosition, txtNamaToko, txtNamaPemilik, txtPhone, txtTanggal;
        RecyclerView recyclerView;
        public SalesHolder(@NonNull View itemView) {
            super(itemView);
            txtNamaPasar = itemView.findViewById(R.id.txtNamaPasar);
            txtPosition = itemView.findViewById(R.id.txtPosition);
            txtNamaToko = itemView.findViewById(R.id.txtNamaToko);
            txtNamaPemilik = itemView.findViewById(R.id.txtNamaPemilik);
            txtPhone = itemView.findViewById(R.id.txtPhone);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
            recyclerView = itemView.findViewById(R.id.recyclerSalesProduct);
        }
    }
}
