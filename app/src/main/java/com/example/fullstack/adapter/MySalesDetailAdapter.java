package com.example.fullstack.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fullstack.R;
import com.example.fullstack.models.SalesDetail;

import java.util.ArrayList;

public class MySalesDetailAdapter extends RecyclerView.Adapter<MySalesDetailAdapter.DetailHolder>{
    Context context;
    ArrayList<SalesDetail> salesDetailArrayList;

    public MySalesDetailAdapter(Context context, ArrayList<SalesDetail> salesDetailArrayList) {
        this.context = context;
        this.salesDetailArrayList = salesDetailArrayList;
    }

    @NonNull
    @Override
    public DetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pengambilan_detail, parent, false);
        DetailHolder detailHolder = new DetailHolder(view);
        return detailHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DetailHolder holder, int position) {
        SalesDetail salesDetail = salesDetailArrayList.get(position);
        holder.txtProduct.setText(salesDetail.getProduct().getName());
        holder.txtQty.setText(salesDetail.getQty());
    }

    @Override
    public int getItemCount() {
        return salesDetailArrayList.size();
    }

    class DetailHolder extends RecyclerView.ViewHolder{
        TextView txtProduct;
        TextView txtQty;
        public DetailHolder(@NonNull View itemView) {
            super(itemView);
            txtProduct = itemView.findViewById(R.id.txtNameProduct);
            txtQty = itemView.findViewById(R.id.txtQty);
        }
    }
}
