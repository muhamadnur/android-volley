package com.example.fullstack.models;

public class Distribution {
    private int id;
    private Area area;
    private String name,segt,email,phone,address;

    public Distribution() {
    }

    public Distribution(int id, Area area, String name, String segt, String email, String phone, String address) {
        this.id = id;
        this.area = area;
        this.name = name;
        this.segt = segt;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSegt() {
        return segt;
    }

    public void setSegt(String segt) {
        this.segt = segt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
