package com.example.fullstack.models;

import java.util.ArrayList;

public class TakingDetail {
    private int id;
    private Taking taking;
    private Product product;
    private User user;
    private String qty, date;
    ArrayList<Product> productArrayList;

    public TakingDetail() {
    }


    public void setProductArrayList(ArrayList<Product> productArrayList) {
        this.productArrayList = productArrayList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Taking getTaking() {
        return taking;
    }

    public void setTaking(Taking taking) {
        this.taking = taking;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
