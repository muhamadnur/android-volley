package com.example.fullstack.models;

import java.util.ArrayList;

public class Sales {
    private int id;
    private User user;
    private ArrayList<SalesDetail> salesDetailArrayList;
    private String created_at;
    JourneyPlan journeyPlan;

    public Sales() {
    }

    public JourneyPlan getJourneyPlan() {
        return journeyPlan;
    }

    public void setJourneyPlan(JourneyPlan journeyPlan) {
        this.journeyPlan = journeyPlan;
    }

    public ArrayList<SalesDetail> getSalesDetailArrayList() {
        return salesDetailArrayList;
    }

    public void setSalesDetailArrayList(ArrayList<SalesDetail> salesDetailArrayList) {
        this.salesDetailArrayList = salesDetailArrayList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
