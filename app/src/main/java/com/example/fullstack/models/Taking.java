package com.example.fullstack.models;

import java.util.ArrayList;

public class Taking {
    int id;
    User user;
    String photo;
    String date;
    ArrayList<TakingDetail> takingDetailArrayList;

    public Taking() {
    }

    public Taking(int id, User user, String photo, String date, ArrayList<TakingDetail> takingDetailArrayList) {
        this.id = id;
        this.user = user;
        this.photo = photo;
        this.date = date;
        this.takingDetailArrayList = takingDetailArrayList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<TakingDetail> getTakingDetailArrayList() {
        return takingDetailArrayList;
    }

    public void setTakingDetailArrayList(ArrayList<TakingDetail> takingDetailArrayList) {
        this.takingDetailArrayList = takingDetailArrayList;
    }
}
